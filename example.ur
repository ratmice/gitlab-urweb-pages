fun main () = return <xml>
	<head>
		<title>Urweb gitlab example</title>
	</head>
	<body>
		<h1>Urweb gitlab example...</h1>
		<p>
		    Intended as an example of using gitlab's continuous integration
		    for generating static sites with urweb for gitlab-pages.
		</p>
		<p>
		    <ol>
		     <li><a href="https://gitlab.com/ratmice/docker-urweb">Docker</a> image</li>
		     <li><a href="https://gitlab.com/ratmice/gitlab-urweb-pages">Page</a> Generation</li>
		    </ol>
		    <p> The docker image gets built by the gitlab CI, then uploaded into the gitlab container registry.
		    <br/>The page generation then gets built by the CI using the docker image as its container.
		    </p>
		</p>
	</body>
</xml>
