![Build Status](https://gitlab.com/ratmice/gitlab-urweb-pages/badges/master/build.svg)

---

Example [Urweb] website using GitLab Pages.

Urweb is not traditionally a static site generator, but can be used as such
by generating a dynamic site then scraping it with a web crawler.

There is a repository which will build a [docker image] containing Urweb and upload it to your [container registry].

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: registry.gitlab.com/ratmice/docker-urweb/urweb

pages:
  script:
  - urweb example
  - ./mirror.sh
  artifacts:
    paths:
    - public 
```


## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Urweb
1. Generate the Urweb server: `urweb projectname`
1. Run the urweb server: `./projectname.exe`
1. Preview your project: [http://localhost:8080/]
1. To generate static pages: `wget --mirror url`
1. Add content

Read more at Urweb's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

[ci]: https://about.gitlab.com/gitlab-ci/
[Urweb]: http://impredicative.com/ur
[install]: http://www.impredicative.com/ur/manual.pdf
[documentation]: http://www.impredicative.com/ur/resources.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
[docker image]: https://gitlab.com/ratmice/docker-urweb
[container registry]: https://docs.gitlab.com/ee/user/project/container_registry.html
----

Forked from @ratmice
