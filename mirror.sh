#!/bin/sh -e

urweb example
./example.exe & echo $! >example.pid
wget -m localhost:8080/index.html
kill -9 $(cat example.pid)
mv localhost:8080 public 
rm -rf example.pid
